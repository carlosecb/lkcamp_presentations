> ls /dev/video0
ls: cannot access '/dev/video0': No such file or directory
> mkdir /configfs/fakevi/my_fake_device_0
> ls /dev/video0
/dev/video0
> tree /configfs/fakevi/my_fake_device_0/
.
|_ image/
|  |_ bright
|  |_ saturation
|  |_ hue
|  |_ shade
|_ filter
> cat /configfs/fakevi/my_fake_device_0/image/bright
22
> echo 42 > /configfs/fakevi/my_fake_device_0/image/bright
> cat /configfs/fakevi/my_fake_device_0/image/bright
42
> echo "sepia" > /configfs/fakevi/my_fake_device_0/filter
> rmdir /configfs/fakevi/my_fake_device_0
